﻿Class MainWindow
    Dim ellipse(120) As Ellipse
    Dim contador As Integer = 0
    Dim Colors(60) As UserControl
    Dim Colors3(45) As UserControl
    Dim cont6 As Integer = 0
    Dim cont3 As Integer = 0
    Dim abc As UserControl


    'este metodo me sirve para darle la estructura a mi juego para que aparezca como una estrella
    '------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------
    Private Sub Estructura()
        Dim left As Integer = 0
        Dim top As Integer = 1

        For y As Integer = 0 To 16
            For x As Integer = 0 To 12
                Dim ini As New Ellipse
                ini.Width = 40
                ini.Height = 40
                ini.Stroke = Brushes.Red
                ini.Fill = Brushes.White
                If y Mod 2 = 0 Then
                    Canvas.SetLeft(ini, CDbl(left))
                Else
                    Canvas.SetLeft(ini, CDbl(left + 20))
                End If
                Canvas.SetTop(ini, CDbl(top))
                Select Case y
                    Case 0, 16
                        If (x = 6) Then
                            Tablero2.Children.Add(ini)
                            ellipse(contador) = ini
                            contador += 1
                        End If
                    Case 1, 15
                        If (x = 6 Or x = 5) Then
                            Tablero2.Children.Add(ini)
                            ellipse(contador) = ini
                            contador += 1
                        End If
                    Case 2, 14
                        If (x = 5 Or x = 6 Or x = 7) Then
                            Tablero2.Children.Add(ini)
                            ellipse(contador) = ini
                            contador += 1
                        End If
                    Case 3, 13
                        If (x = 4 Or x = 5 Or x = 6 Or x = 7) Then
                            Tablero2.Children.Add(ini)
                            ellipse(contador) = ini
                            contador += 1
                        End If
                    Case 4, 12
                        Tablero2.Children.Add(ini)
                        ellipse(contador) = ini
                        contador += 1
                    Case 5, 11
                        If Not (x = 12) Then
                            ellipse(contador) = ini
                            Tablero2.Children.Add(ini)
                            contador += 1
                        End If
                    Case 6, 10
                        If Not (x = 12 Or x = 0) Then
                            Tablero2.Children.Add(ini)
                            ellipse(contador) = ini
                            contador += 1
                        End If
                    Case 7, 9
                        If Not (x = 12 Or x = 0 Or x = 11) Then
                            Tablero2.Children.Add(ini)
                            ellipse(contador) = ini
                            contador += 1
                        End If
                    Case 8
                        If Not (x = 12 Or x = 0 Or x = 11 Or x = 1) Then
                            Tablero2.Children.Add(ini)
                            ellipse(contador) = ini
                            contador += 1
                        End If
                End Select
                left += 40
            Next
            top += 40
            left = 0
        Next
    End Sub


    ' este metodo me sirve para agregar colores distintos a cada parte de la estructura de mi proyecto ya que el juego debe de ser de varios colores funcionara solo cuando la catidad de jugadoressea de 6
    '------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------

    Private Sub colocarBolitas()
        Dim left As Double
        Dim top As Double

        For a As Integer = 46 To 46
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Blue
            Dim azul As New azul
            Canvas.SetLeft(azul, left)
            Canvas.SetTop(azul, top)
            Tablero2.Children.Add(azul)
            Colors(cont6) = azul
            cont6 += 1
        Next
        For a As Integer = 55 To 55
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Green
            Dim verde As New verde
            Canvas.SetLeft(verde, left)
            Canvas.SetTop(verde, top)
            Tablero2.Children.Add(verde)
            Colors(cont6) = verde
            cont6 += 1
        Next
        For a As Integer = 65 To 65
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Yellow
            Dim amarillo As New amarillo
            Canvas.SetLeft(amarillo, left)
            Canvas.SetTop(amarillo, top)
            Tablero2.Children.Add(amarillo)
            Colors(cont6) = amarillo
            cont6 += 1
        Next
        For a As Integer = 74 To 74
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Red
            Dim rojo As New rojo
            Canvas.SetLeft(rojo, left)
            Canvas.SetTop(rojo, top)
            Tablero2.Children.Add(rojo)
            Colors(cont6) = rojo
            cont6 += 1
        Next
        For a As Integer = 75 To 76
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Yellow
            Dim amarillo As New amarillo
            Canvas.SetLeft(amarillo, left)
            Canvas.SetTop(amarillo, top)
            Tablero2.Children.Add(amarillo)
            Colors(cont6) = amarillo
            cont6 += 1

        Next
        For a As Integer = 86 To 88
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Yellow
            Dim amarillo As New amarillo
            Canvas.SetLeft(amarillo, left)
            Canvas.SetTop(amarillo, top)
            Tablero2.Children.Add(amarillo)
            Colors(cont6) = amarillo
            cont6 += 1

        Next
        For a As Integer = 98 To 101
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Yellow
            Dim amarillo As New amarillo
            Canvas.SetLeft(amarillo, left)
            Canvas.SetTop(amarillo, top)
            Tablero2.Children.Add(amarillo)
            Colors(cont6) = amarillo
            cont6 += 1

        Next
        For a As Integer = 84 To 85
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Red
            Dim rojo As New rojo
            Canvas.SetLeft(rojo, left)
            Canvas.SetTop(rojo, top)
            Tablero2.Children.Add(rojo)
            Colors(cont6) = rojo
            cont6 += 1

        Next
        For a As Integer = 107 To 110
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Red
            Dim rojo As New rojo
            Canvas.SetLeft(rojo, left)
            Canvas.SetTop(rojo, top)
            Tablero2.Children.Add(rojo)
            Colors(cont6) = rojo
            cont6 += 1
        Next
        For a As Integer = 95 To 97
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Red
            Dim rojo As New rojo
            Canvas.SetLeft(rojo, left)
            Canvas.SetTop(rojo, top)
            Tablero2.Children.Add(rojo)
            Colors(cont6) = rojo
            cont6 += 1
        Next
        For a As Integer = 10 To 13
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Blue
            Dim azul As New azul
            Canvas.SetLeft(azul, left)
            Canvas.SetTop(azul, top)
            Tablero2.Children.Add(azul)
            Colors(cont6) = azul
            cont6 += 1
        Next
        For a As Integer = 0 To 9
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Black
            Dim negro As New negro
            Canvas.SetLeft(negro, left)
            Canvas.SetTop(negro, top)
            Tablero2.Children.Add(negro)
            Colors(cont6) = negro
            cont6 += 1
        Next
        For a As Integer = 22 To 25
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Blue
            Dim azul As New azul
            Canvas.SetLeft(azul, left)
            Canvas.SetTop(azul, top)
            Tablero2.Children.Add(azul)
            Colors(cont6) = azul
            cont6 += 1
        Next
        For a As Integer = 35 To 36
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Blue
            Dim azul As New azul
            Canvas.SetLeft(azul, left)
            Canvas.SetTop(azul, top)
            Tablero2.Children.Add(azul)
            Colors(cont6) = azul
            cont6 += 1
        Next
        For a As Integer = 19 To 22
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Green
            Dim verde As New verde
            Canvas.SetLeft(verde, left)
            Canvas.SetTop(verde, top)
            Tablero2.Children.Add(verde)
            Colors(cont6) = verde
            cont6 += 1
        Next
        For a As Integer = 44 To 45
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Green
            Dim verde As New verde
            Canvas.SetLeft(verde, left)
            Canvas.SetTop(verde, top)
            Tablero2.Children.Add(verde)
            Colors(cont6) = verde
            cont6 += 1
        Next
        For a As Integer = 32 To 34
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Green
            Dim verde As New verde
            Canvas.SetLeft(verde, left)
            Canvas.SetTop(verde, top)
            Tablero2.Children.Add(verde)
            Colors(cont6) = verde
            cont6 += 1
        Next
        For a As Integer = 111 To 120
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Aqua
            Dim aqua As New aqua
            Canvas.SetLeft(aqua, left)
            Canvas.SetTop(aqua, top)
            Tablero2.Children.Add(aqua)
            Colors(cont6) = aqua
            cont6 += 1
        Next
        ' MsgBox(Canvas.GetLeft(elliplse(0)))
    End Sub

    '------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------

    Private Sub mover6()
        If txtnumero.Text = 3 Then
            For o As Integer = 0 To 45
                AddHandler Colors3(o).MouseUp, AddressOf click
            Next
        ElseIf txtnumero.Text = 6 Then
            For o As Integer = 0 To 60
                AddHandler Colors(o).MouseUp, AddressOf click
            Next
        End If
        For o As Integer = 0 To 120
            AddHandler ellipse(o).MouseUp, AddressOf hola
        Next
    End Sub
    Private Sub click(ByVal sender As Object, ByVal e As EventArgs)
        Dim posX As Integer = 0
        Dim posY As Integer = 0
        For a As Integer = 0 To 60
            If Colors(a) Is sender Then
                posX = Canvas.GetLeft(Colors(a))
                posY = Canvas.GetTop(Colors(a))
                Rojo()
                cambio(posX + 40, posY)
                cambio(posX - 40, posY)
                cambio(posX + 20, posY + 40)
                cambio(posX - 20, posY + 40)
                cambio(posX + 20, posY - 40)
                cambio(posX - 20, posY - 40)
                abc = Colors(a)
            End If
        Next
    End Sub

    '------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------

    Private Sub hola(ByVal sender As Object, ByVal e As EventArgs)
        For a As Integer = 0 To 120
            If ellipse(a) Is sender Then
                If ellipse(a).Fill.ToString = "#FF0000FF" Then
                    Canvas.SetLeft(abc, Canvas.GetLeft(ellipse(a)))
                    Canvas.SetTop(abc, Canvas.GetTop(ellipse(a)))
                    Rojo()
                Else
                    MsgBox("No es un posible movimiento")
                End If
            End If
        Next
    End Sub

    '------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------

    Private Sub cambio(ByVal posX As Integer, ByVal posY As Integer)
        For a As Integer = 0 To 120
            If Canvas.GetLeft(ellipse(a)) = posX And Canvas.GetTop(ellipse(a)) = posY Then
                ellipse(a).Fill = Brushes.Blue
            End If
        Next
    End Sub


    '------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------
    Private Sub Rojo()
        For o As Integer = 0 To 120
            ellipse(o).Fill = Brushes.White
        Next
    End Sub



    ' este metodo es para  colocar las piezas cuando se haya seleccionado jugar con tres integrantes
    '------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------

    Private Sub ColorBolitas_3()
        Dim left As Double
        Dim top As Double
        For a As Integer = 0 To 9
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Black
            Dim negro As New negro
            Canvas.SetLeft(negro, left)
            Canvas.SetTop(negro, top)
            Tablero2.Children.Add(negro)
            Colors3(cont3) = negro
            cont3 += 1
        Next
        For a As Integer = 14 To 18
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Black
            Dim negro As New negro
            Canvas.SetLeft(negro, left)
            Canvas.SetTop(negro, top)
            Tablero2.Children.Add(negro)
            Colors3(cont3) = negro
            cont3 += 1
        Next
        For a As Integer = 56 To 56
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Yellow
            Dim amarillo As New amarillo
            Canvas.SetLeft(amarillo, left)
            Canvas.SetTop(amarillo, top)
            Tablero2.Children.Add(amarillo)
            Colors3(cont3) = amarillo
            cont3 += 1
        Next
        For a As Integer = 65 To 66
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Yellow
            Dim amarillo As New amarillo
            Canvas.SetLeft(amarillo, left)
            Canvas.SetTop(amarillo, top)
            Tablero2.Children.Add(amarillo)
            Colors3(cont3) = amarillo
            cont3 += 1
        Next
        For a As Integer = 75 To 77
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Yellow
            Dim amarillo As New amarillo
            Canvas.SetLeft(amarillo, left)
            Canvas.SetTop(amarillo, top)
            Tablero2.Children.Add(amarillo)
            Colors3(cont3) = amarillo
            cont3 += 1

        Next
        For a As Integer = 86 To 89
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Yellow
            Dim amarillo As New amarillo
            Canvas.SetLeft(amarillo, left)
            Canvas.SetTop(amarillo, top)
            Tablero2.Children.Add(amarillo)
            Colors3(cont3) = amarillo
            cont3 += 1

        Next
        For a As Integer = 98 To 102
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Yellow
            Dim amarillo As New amarillo
            Canvas.SetLeft(amarillo, left)
            Canvas.SetTop(amarillo, top)
            Tablero2.Children.Add(amarillo)
            Colors3(cont3) = amarillo
            cont3 += 1
        Next
        For a As Integer = 64 To 64
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Red
            Dim rojo As New rojo
            Canvas.SetLeft(rojo, left)
            Canvas.SetTop(rojo, top)
            Tablero2.Children.Add(rojo)
            Colors3(cont3) = rojo
            cont3 += 1
        Next
        For a As Integer = 73 To 74
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Red
            Dim rojo As New rojo
            Canvas.SetLeft(rojo, left)
            Canvas.SetTop(rojo, top)
            Tablero2.Children.Add(rojo)
            Colors3(cont3) = rojo
            cont3 += 1
        Next
        For a As Integer = 83 To 85
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Red
            Dim rojo As New rojo
            Canvas.SetLeft(rojo, left)
            Canvas.SetTop(rojo, top)
            Tablero2.Children.Add(rojo)
            Colors3(cont3) = rojo
            cont3 += 1

        Next
        For a As Integer = 106 To 110
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Red
            Dim rojo As New rojo
            Canvas.SetLeft(rojo, left)
            Canvas.SetTop(rojo, top)
            Tablero2.Children.Add(rojo)
            Colors3(cont3) = rojo
            cont3 += 1

        Next
        For a As Integer = 94 To 97
            left = Canvas.GetLeft(ellipse(a))
            top = Canvas.GetTop(ellipse(a))
            ellipse(a).Fill = Brushes.Red
            Dim rojo As New rojo
            Canvas.SetLeft(rojo, left)
            Canvas.SetTop(rojo, top)
            Tablero2.Children.Add(rojo)
            Colors3(cont3) = rojo
            cont3 += 1
        Next
    End Sub


    'este metodo me ayudara a escoger la cantidad de jugadores con la que deseo jugar
    '------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------

    Private Sub cant_jugadores(ByVal sender As Object, ByVal e As EventArgs) Handles btnjugar.Click
        If txtnumero.Text = "3" Then
            Estructura()
            ColorBolitas_3()
        Else
            If txtnumero.Text = "6" Then
                Estructura()
                colocarBolitas()
                mover6()
            Else
                If txtnumero.Text <> "3" Or "6" Then
                    MsgBox("el numero de jugdores que usted desea no esta permitido")
                End If
            End If
        End If
    End Sub


    'este metodo inicia mi proyecto o mi tablero de juego
    'Private Sub Tablero_Initialized(sender As Object, e As EventArgs) Handles Tablero2.Initialized
    '    Estructura()
    '    colocarBolitas()
    'End Sub
    '------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------

    Private Sub Salir(ByVal sender As Object, ByVal e As EventArgs) Handles btnsalir.Click
        Close()
    End Sub

    ' este metodo es para iniciar una nueva partida en el tablero de juego nos permitira regresar hasta las opciones de numero de jugadores
    '------------------------------------------------------------------------------------------------
    '------------------------------------------------------------------------------------------------

    Private Sub NuevaPartida(ByVal sender As Object, ByVal e As EventArgs) Handles btnnuevapartida.Click
        Tablero2.Children.Clear()
        MsgBox("iniciando partida")
        contador = 0

    End Sub

    Private Sub Button_Click_1(sender As Object, e As RoutedEventArgs)

    End Sub
End Class
